#!/bin/bash

set -euxo pipefail

rm -f */*.tar.gz *.out failed.log
