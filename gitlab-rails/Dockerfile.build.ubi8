ARG BUILD_IMAGE=
ARG ASSETS_IMAGE=

FROM ${ASSETS_IMAGE} AS assets
FROM ${BUILD_IMAGE}

ARG NAMESPACE=gitlab-org
ARG PROJECT=gitlab
ARG VERSION=v12.0.2-ee
ARG API_URL=
ARG API_TOKEN=

ARG NODE_VERSION=12.9.1
ARG YARN_VERSION=1.17.3
ARG LIBRE2_VERSION=2019-09-01
ARG GITLAB_EDITION=gitlab-ee
ARG GITLAB_USER=git

ADD gitlab-ruby.tar.gz /
ADD postgresql.tar.gz /
ADD gitlab-elasticsearch-indexer.tar.gz /

ENV PRIVATE_TOKEN=${API_TOKEN}

RUN mkdir /assets \
    && cp /usr/local/postgresql/bin/* /usr/bin/ \
    && cp -R /usr/local/postgresql/lib/* /usr/lib/ \
    && cp -R /usr/local/postgresql/include/* /usr/include/ \
    && cp -R /usr/local/postgresql/share/* /usr/share/ \
    && curl -L https://github.com/google/re2/archive/${LIBRE2_VERSION}.tar.gz | tar -xz \
    && cd re2-${LIBRE2_VERSION} \
    && make \
    && make install \
    && mkdir -p /libre2/{lib,include} \
    && cp -R /usr/local/lib/{libre2.*,pkgconfig/} /libre2/lib \
    && cp -R /usr/local/include/re2 /libre2/include/ \
    && curl -L https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar --strip-components 1 -xzC /usr/local/ \
    && mkdir /usr/local/yarn \
    && curl -L https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz | tar -xzC /usr/local/yarn --strip 1 \
    && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin \
    && printf 'install: --no-document\nupdate: --no-document\n' > ~/.gemrc

ENV RAILS_ENV=production
ENV NODE_ENV=production
ENV USE_DB=false
ENV SKIP_STORAGE_VALIDATION=true

COPY shared/build-scripts/ /build-scripts

RUN /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${VERSION}" \
    && cd ${PROJECT}-${VERSION} \
    && printf ${GITLAB_EDITION} | cut -d '-' -f 2 > REVISION \
    && printf 'gitlab-cloud-native-image' > INSTALLATION_TYPE \
    && for cfg in config/{gitlab,resque,secrets,database}.yml config/initializers/rack_attack.rb; do cp ${cfg}.example ${cfg} ; done \
    && cp config/database.yml.postgresql config/database.yml \
    && ln -s ./initializers/rack_attack.rb config/rack_attack.rb  \
    && bundle install --deployment --without development test mysql aws kerberos --jobs $(nproc) --retry 5 \
    && yarn install --production --pure-lockfile \
    && rm -rf node_modules/ tmp/ spec/ \
    && /build-scripts/cleanup-gems \
    && mv ${PWD} /srv/gitlab

COPY --from=assets assets /srv/gitlab/public/assets/

RUN mkdir -p /assets/usr/{bin,lib} \
    && cp /usr/local/bin/gitlab-elasticsearch-indexer /assets/usr/bin/ \
    && cp /usr/local/postgresql/bin/* /assets/usr/bin/ \
    && cp -R /usr/local/postgresql/lib/* /assets/usr/lib/ \
    && cp -R --parents \
        /usr/local/lib/libre2.* \
        /srv/gitlab \
        /assets
